import { useState, useEffect } from 'react';
import { carregaNotificacoes } from '../servicos/carregaDados';

export default function useNotificacoes() {
    const [lista, setLista] = useState([]);

    useEffect(()=>{
        updateData();
    },[]);

    function updateData() {
        const retorno = carregaNotificacoes();
        setLista(retorno.lista);
    }

    return [lista, updateData]
}