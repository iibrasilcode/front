import { useState, useEffect } from 'react';
import { carregaEntradas } from '../servicos/carregaDados';

export default function useEntradas() {
    const [titulo, setTitulo] = useState('');
    const [descricao, setDescricao] = useState('');
    const [lista, setLista] = useState([]);
    const [total, setTotal] = useState('');

    useEffect(()=>{
        updateData();
    },[]);

    function updateData() {
        const retorno = carregaEntradas();
        var total = 0
        retorno.lista.forEach(item => {
            total+=parseFloat(item.valor);
        })

        setTitulo(retorno.titulo);
        setDescricao(retorno.descricao);
        setLista(retorno.lista);
        setTotal(total.toFixed(2));
    }

    return [titulo, descricao, lista, total, updateData]
}