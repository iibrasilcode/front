import React, {useEffect} from 'react';
import {SafeAreaView, StyleSheet, StatusBar} from 'react-native';

import AppRotas from './src/rotas/AppRotas';
import SplashScreen from 'react-native-splash-screen';

function App() {
  useEffect(() => {
    SplashScreen.hide(); //hides the splash screen on app load.
  }, []);
  
  return (
  <SafeAreaView style={style.tela} >
    <StatusBar backgroundColor={"#eee"} barStyle={'dark-content'}/>
    <AppRotas/>
  </SafeAreaView>
  )
}

const style = StyleSheet.create({
  tela: {
    flex: 1,
  },
})

export default App;