# React Native CLI

Siga o [tutorial de instalação do React Native CLI](https://reactnative.dev/docs/environment-setup)
#### Importante: Instalar a versão CLI

### Caso o projeto possua algum módulo instalado, instale usando:

```shell
npm install nome-do-modulo
```

Ex: ```npm install react-native-webview```

## Inicializando o App

#### Primeiro inicializar o Metro (ambas as plataformas):

###### Executar na raíz do projeto: 

```npx react-native start```

##### Para Android:

```npx react-native run-android```

Caso queira rodar no próprio celular e não pelo emulador, ative a Depuração USB nas configurações de desenvolvedor do celular, use o comando ```adb devices``` para pegar o ID do aparelho, e então o comando será:

```npx react-native run-android --deviceId=ID_DO_APARELHO```

##### Para iOS:

```npx react-native run-ios```

## Compilando o app para Android

### Na pasta android, utilize o seguinte comando:

```./gradlew assembleRelease``` gera o arquivo apk

```./gradlew bundleRelease``` gera o arquivo aab

Os arquivos podem ser encontrado em ```android/app/build/outputs```.