import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Home from '../telas/Home';
import Entradas from '../telas/Home/Entradas';
import Saidas from '../telas/Home/Saidas';
import Resultado from '../telas/Home/Resultado';
import Notificacoes from '../telas/Home/Notificacoes';

const Stack = createNativeStackNavigator();

export default function HomeRotas() {
    return (
        <Stack.Navigator
        screenOptions={{headerShown: false, animation: 'slide_from_bottom', presentation: 'modal'}}>
            <Stack.Screen name={"Home"} component={Home} />
            <Stack.Screen name={"Entradas"} component={Entradas} />
            <Stack.Screen name={"Saídas"} component={Saidas} />
            <Stack.Screen name={"Resultado"} component={Resultado} />
            <Stack.Screen name={"Notificações"} component={Notificacoes}/>
        </Stack.Navigator>
    )
}