import React from 'react';
import {Image} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

//Rota e tela
import HomeRotas from './HomeRotas';
import Transacoes from '../telas/Transacoes';
import NotaFiscal from '../telas/NotaFiscal';
import Perfil from '../telas/Perfil';

import Login from '../telas/Login';

import { Cores } from '../Cores';

const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

const screenOptions = (route, color) => {
  let icon;

  switch (route.name) {
    case 'Início':
      icon = require('../assets/icons/home.png');
      break;
    case 'Transações':
      icon = require('../assets/icons/document.png');
      break;
    case 'Nota Fiscal':
      icon = require('../assets/icons/qr-code.png');
      break;
    case 'Perfil':
      icon = require('../assets/icons/user.png');
      break;
    default:
      break;
  }

  return <Image source={icon} style={{tintColor: color, height: 24, width: 24}}/>
};

function HomeTabs() {
  return (
    <Tab.Navigator
        screenOptions={({route}) => ({
          tabBarIcon: ({color}) => screenOptions(route, color),
          tabBarActiveTintColor: Cores.primary,
          tabBarInactiveTintColor: "#888",
          tabBarStyle: {
            height: 53,
            paddingBottom: 3,
            borderTopLeftRadius: 10,
            borderTopRightRadius: 10,
          },
          tabBarLabelStyle: {
            fontWeight: "500",
          },
          headerShown: false,
          unmountOnBlur: true,
          tabBarShowLabel: true,
        })}>
      <Tab.Screen name={"Início"} component={HomeRotas}/>
      <Tab.Screen name={"Transações"} component={Transacoes}/>
      <Tab.Screen name={"Nota Fiscal"} component={NotaFiscal}/>
      <Tab.Screen name={"Perfil"} component={Perfil}/>
    </Tab.Navigator>
  );
}


export default function AppRotas() {
  return ( 
  <NavigationContainer>
    <Stack.Navigator screenOptions={
      {
        headerShown: false, 
        orientation: 'portrait_up', 
        animation: 'fade_from_bottom',
        //navigationBarColor: "#eee",
        }}>
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Home" component={HomeTabs} />
    </Stack.Navigator>
  </NavigationContainer>)
}