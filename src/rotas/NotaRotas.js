import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import TelaAdicionar from '../telas/NotaFiscal/componentes/TelaAdicionar';
import Camera from '../telas/NotaFiscal/componentes/Camera';

const Stack = createNativeStackNavigator();

export default function NotaRotas() {
    return (
        <Stack.Navigator
        screenOptions={{headerShown: false, animation: 'fade_from_bottom', presentation: 'modal'}}>
            <Stack.Screen name={"Camera"} component={Camera} />
            <Stack.Screen name={"TelaAdicionar"} component={TelaAdicionar} />
        </Stack.Navigator>
    )
}