export const Cores = {
    primary: "#14213d",
    primaryDarker: "#10192E",
    secondary: "",
    backgroundLista: "#F9F9FB",
    description: {
        primary: "#8E919A",
        secondary: "#B5B8C2",
    },

    //entradas e saídas
    money: {
        in: "#17A73D",
        out: "#db4444",
    },

    //cores de caixa de texto
    input: {
        placeholder: "#CFD1D7",
        selection: "#4A7AE0",
        background: "#F9F9FB",
    }

}