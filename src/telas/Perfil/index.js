import React, {useState} from 'react';
import { ScrollView, StyleSheet, Switch, View} from 'react-native';
import Header from '../Perfil/componentes/Header';
import Item from './componentes/Item';
import { useNavigation, StackActions } from '@react-navigation/native';

import { Cores } from '../../Cores';

export default function Perfil() {
    const navigation = useNavigation();
    const [isEnabled, setIsEnabled] = useState(false);
    const toggleSwitch = () => setIsEnabled(previousState => !previousState);

    function replaceScreen(screen) {
        navigation.dispatch(
            StackActions.replace(screen)
        );
    }

    return (
    <ScrollView style={{flex:1}}>
        <Header/>
        <View style={style.container}>

            {/* <View style={{flexDirection: "row", justifyContent: "space-between"}}>
                <Text style={style.text}>Tema escuro</Text>
                <Switch
                trackColor={{ false: "#767577", true: "#81b0ff" }}
                thumbColor={isEnabled ? Cores.primary : "#f4f3f4"}
                ios_backgroundColor="#3e3e3e"
                onValueChange={toggleSwitch}
                value={isEnabled}/>
            </View> */}

            <Item text={"Configurações"}/>
            <Item text={"Sair"} onPress={()=>replaceScreen("Login")}/>
        </View>
    </ScrollView>
    )
}

const style = StyleSheet.create({
    container: {
        padding: 16,
    },
    text: {
        fontSize: 16,
        color: Cores.primary,
    },
})