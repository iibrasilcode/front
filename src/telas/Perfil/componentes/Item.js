import React from 'react';
import {StyleSheet, TouchableOpacity, Text} from 'react-native';

import { Cores } from '../../../Cores';

export default function Item({text="text", onPress}) {
    return(
        <TouchableOpacity onPress={onPress} activeOpacity={0.7} style={style.botao} >
            <Text style={style.texto}>{text}</Text>
        </TouchableOpacity>
    )
}

const style = StyleSheet.create({
    botao: {
        paddingVertical: 20,
        paddingHorizontal: 10,
        borderColor: "#dddddd",
        borderBottomWidth: 1,
    }, 
    texto: {
        color: Cores.primary,
        fontSize: 16,
    }
})