import React from 'react';
import {StyleSheet, View, Text, Image, StatusBar, TouchableHighlight} from 'react-native';

import { Cores } from '../../../Cores';

export default function Header() {
    return (
    <>
    <StatusBar backgroundColor={"#d3d3d3"} barStyle={'dark-content'}/>
    <View style={{width: "100%", height: 160, position: "absolute", backgroundColor: "#dfdfdf"}}/>
    <View style={style.header}>
        
        <View>
            <Image source={require('../../../assets/Home/profile.jpg')} style={style.imagem}/>
            <TouchableHighlight style={style.cameraBtn} onPress={()=>alert("Editar foto")}>
                <Image source={require('../../../assets/icons/camera.png')} style={style.camera}/>
            </TouchableHighlight>
        </View>
        <Text style={style.nome}>{global.usuario}</Text>
    </View>
    </>
    )
}

const style = StyleSheet.create({
    header: {
        alignItems: "center",
        padding: 10,
        marginTop: 30,
    },
    imagem: {
        width: 170,
        height: 170,
        borderRadius: 170/2,
        borderWidth: 4,
        borderColor: Cores.primary
    },
    nome: {
        fontSize: 26,
        fontWeight: "bold",
        color: Cores.primary,
        marginTop: 10,
    },
    camera :{
        height: 25,
        width: 25,
        tintColor: "#fff",
    },
    cameraBtn: {
        position: 'absolute',
        bottom: 2,
        right: 10,
        backgroundColor: Cores.primary,
        borderRadius: 12,
        padding: 5,
    }
})