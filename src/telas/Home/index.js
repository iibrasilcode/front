import React from 'react';
import { ScrollView, StyleSheet, View, RefreshControl} from 'react-native';

import HeaderHome from './componentes/HeaderHome';

import Entradas from './componentes/Entradas';
import Saidas from './componentes/Saidas';
import Resultado from './componentes/Resultado';
import CardPequeno from './componentes/CardPequeno';

import useEntradas from '../../../hooks/useEntradas';
import useSaidas from '../../../hooks/useSaidas';

import { Cores } from '../../Cores';
import { useNavigation } from '@react-navigation/native';

export default function Home() {
    const navigation = useNavigation();
    const [tituloEntrada, descricaoEntrada, listaEntrada, totalEntrada, updateEntradas] = useEntradas();
    const [tituloSaida, descricaoSaida, listaSaida, totalSaida, updateSaidas] = useSaidas();

    const [refreshing, setRefreshing] = React.useState(false);
    const [modalVisible, setModal] = React.useState(false);

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        setTimeout(() => {
            updateEntradas();
            updateSaidas();
            setRefreshing(false);
        }, 700);
    }, []);

    React.useEffect(() => {
        return navigation.addListener('focus', () => {
            updateEntradas();
            updateSaidas();
        });
    }, [navigation]);

    return (
    <View style={{flex:1}}>
        <ScrollView style={style.container} contentContainerStyle={{flexGrow: 1}}
        refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh}/>}>
            <HeaderHome setModal={setModal}/>
            <View style={{padding: 10}}>
                <Entradas entradas={totalEntrada}/>
                <Saidas saidas={totalSaida}/>
                <Resultado resultado={(totalEntrada-totalSaida).toFixed(2)}/>
                <CardPequeno icon={require('../../assets/icons/pie-chart-report.png')} titulo={"Gerar Relatório"} onPress={()=>alert("Gerar Relatório")}/>
                <CardPequeno icon={require('../../assets/icons/tax.png')} titulo={"Gerar Imposto de Renda"} onPress={()=>alert("Gerar Imposto de Renda")}/>
            </View>
        </ScrollView>
    </View>
    )
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Cores.backgroundLista,
    }
})