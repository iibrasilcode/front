import React from 'react';
import {Text, StyleSheet, View} from 'react-native';

import { Cores } from '../../../Cores';

export default function Total({texto, valor, ano, corTexto = Cores.primary, corValor}) {
    return (
    <View style={{flexDirection: "row", justifyContent:"space-between", marginHorizontal: 10}}>
        <View style={{flexDirection: "row"}}>
            <Text style={[style.texto, {color: corTexto}]}>{texto}</Text>
            <Text style={[style.texto, {color: corValor}]}>{valor}</Text>
        </View>
        <Text style={[style.texto, {color: corTexto, textAlign: "right"}]}>{ano}</Text>
    </View>
    )
}

const style = StyleSheet.create({
    texto: {
        fontSize: 14,
        fontWeight: "600",
    }
})