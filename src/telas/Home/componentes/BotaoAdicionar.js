import React from 'react';
import {TouchableOpacity, Text, StyleSheet} from 'react-native';

import { Cores } from '../../../Cores';

export default function BotaoAdicionar({texto, valor,onPress}) {
    return (
    <TouchableOpacity style={style.addBtn} onPress={onPress} activeOpacity={0.7}>
        <Text style={style.addTexto}>{texto}</Text>
        <Text style={style.addTexto}>{valor}</Text>
    </TouchableOpacity>    
    )
}

const style = StyleSheet.create({
    addBtn: {
        width: 190,
        alignSelf: "center",
        alignContent: "center",
        padding: 14,
        marginVertical: 20,
        borderRadius: 10,
        backgroundColor: "#ffffff" ,
    },
    addTexto: {
        textAlign: "center",
        fontSize: 16,
        fontWeight: "bold",
        color: Cores.primary,
    }
})