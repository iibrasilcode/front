import React from 'react';
import Card from './Card';
import { useNavigation } from '@react-navigation/native';

export default function Saidas({saidas}) {
    const navigation = useNavigation();
    var date = new Date().toLocaleDateString('pt-BR');

    return <Card icon={require('../../../assets/icons/withdrawal.png')} titulo={"Saídas"} valor={'- R$ '+saidas} atualizacao={date} onPress={()=>{navigation.navigate('Saídas')}}/>
}