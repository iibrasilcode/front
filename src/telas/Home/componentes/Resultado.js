import React from 'react';
import Card from './Card';
import { useNavigation } from '@react-navigation/native';

export default function Resultado({resultado}) {
    const navigation = useNavigation();
    var date = new Date().toLocaleDateString('pt-BR');

    return <Card icon={require('../../../assets/icons/us-dollar-circled.png')} titulo={"Resultado"} valor={'R$ '+resultado} atualizacao={date} onPress={()=> navigation.navigate("Resultado")}/>
}