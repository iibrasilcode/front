import React from 'react';
import {Text, View, StyleSheet, Image, TouchableOpacity} from 'react-native';
import { useNavigation } from '@react-navigation/native';

import { Cores } from '../../../Cores';

export default function Header({icon=require('../../../assets/icons/close.png'), titulo="titulo", descricao="descrição", azul=false}) {
    const navigation = useNavigation();
    const style = styleFunc(azul);
    return (
    <View style={style.header}>
        <View style={style.texto}>
            <Image source={icon} style={style.icon}/>
            <Text style={style.titulo}>{titulo}</Text>
            <Text style={style.descricao}>{descricao}</Text>
        </View>
        <TouchableOpacity style={style.closeBtn} onPress={(navigation.goBack)} activeOpacity={0.7}>
            <Image source={require('../../../assets/icons/close.png')} style={style.closeImg}/>    
        </TouchableOpacity>
    </View>
    )
}

const styleFunc = (azul) => StyleSheet.create({
    header: {
        padding: 20,
        paddingTop: 50,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        width: "100%",
        backgroundColor: !azul? "#ffffff":Cores.primary,
    },
    icon: {
        tintColor: !azul? Cores.primary:"#ffffff",
        alignSelf: "center",
        height: 24,
        width: 24,
    },
    texto: {
        width: "100%",
    },
    titulo: {
        textAlign: "center",
        fontSize: 18,
        fontWeight: "bold",
        color: !azul?Cores.primary:"#ffffff",
    },
    descricao: {
        textAlign: "center",
        color: !azul?Cores.description.primary:Cores.description.secondary,
        fontSize: 14,
        fontWeight: "500",
    },
    closeBtn: {
        position: 'absolute',
        right: 20,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#ffffff",
        width: 42,
        height: 42,
        borderRadius: 10,
        elevation: 8,
    },
    closeImg: {
        tintColor: Cores.primary,
        height: 24,
        width: 24,
    }
})