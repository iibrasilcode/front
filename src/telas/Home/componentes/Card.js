import React from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';

import { Cores } from '../../../Cores';

export default function Card({icon, titulo, valor, atualizacao, onPress}){
    const style = styleFunc(titulo);

    return (
    <TouchableOpacity style={style.card} onPress={onPress} activeOpacity={0.8}>
        <View>
            <View style={{flexDirection:"row", alignItems:"center"}}>
                <Image source={icon} style={style.icon}/>
                <Text style={style.titulo}>{titulo}</Text>
            </View>
            <Text style={style.valor}>{valor}</Text>
            <Text style={style.atualizacao}>Última atualização: {atualizacao}</Text>
        </View>
    </TouchableOpacity>
    )
}

const styleFunc = (titulo) => StyleSheet.create({
    card: {
        width: "100%",
        backgroundColor: (titulo=="Resultado")? Cores.primary : "#ffffff",
        borderRadius: 10,
        padding: 20,
        elevation: 5,
        marginBottom: 20,
    },
    icon: {
        tintColor: (titulo=="Resultado") ? "#ffffff" : Cores.primary,
        height: 24,
        width: 24,
    },
    titulo:{
        marginLeft: 10,
        fontSize: 18,
        fontWeight: "bold",
        color: (titulo=="Resultado") ? "#ffffff" : Cores.primary,
    },
    valor: {
        fontSize: 16,
        fontWeight: "500",
        lineHeight: 20,
        marginVertical: 10,
        marginLeft: 35,
        color: (titulo=="Entradas") ? Cores.money.in : (titulo=="Saídas") ? Cores.money.out : "#ffffff",
    },
    atualizacao: {
        fontSize: 12,
        textAlign: "right",
        color: (titulo=="Resultado") ? "#ffffff" : Cores.primary,
        fontWeight: "500",
    },
})