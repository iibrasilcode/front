import React from 'react';
import {Text,View,StyleSheet, Image, TouchableOpacity} from 'react-native';
import { useNavigation } from '@react-navigation/native';

import { Cores } from '../../../Cores';

export default function HeaderHome({setModal}) {
    const navigation = useNavigation();
    return (
    <View style={style.header}>
        <TouchableOpacity onPress={()=>navigation.navigate("Perfil")} activeOpacity={0.8}>
            <Image source={require('../../../assets/Home/profile.jpg')} style={style.profileImage}/>
        </TouchableOpacity>
        <View style={style.texto}>
            <Text style={style.nome}>Olá, {global.usuario}</Text>
            <Text style={style.bemVindo}>Bem vindo de volta!</Text>
        </View>
        <TouchableOpacity style={style.notificationBtn} onPress={()=>navigation.navigate('Notificações')} activeOpacity={0.7}>
            <Image source={require('../../../assets/icons/notification.png')} style={style.notificationImg}/>
        </TouchableOpacity>
    </View>
    )
}

const style = StyleSheet.create({
    header: {
        padding: 20,
        paddingTop: 50,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        width: "100%",
        backgroundColor: "#FFFFFF",
        elevation: 3,
    },
    profileImage: {
        width: 42,
        height: 42,
        borderRadius: 21,
        borderWidth: 1,
        borderColor: Cores.primary
    },
    texto: {
        textAlign: "center",
    },
    nome: {
        fontSize: 26,
        fontWeight: "bold",
        color: Cores.primary,
        maxWidth: 250,
        textAlign: 'center',
    },
    bemVindo: {
        color: Cores.primary,
        fontSize: 14,
        fontWeight: "500",
        alignSelf: 'center',
    },
    notificationBtn: {
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#ffffff",
        width: 42,
        height: 42,
        borderRadius: 10,
        elevation: 8,
    },
    notificationImg: {
        tintColor: Cores.primary,
        height: 24,
        width: 24,
    }
})