import React from 'react';
import {StyleSheet, Text, Image, TouchableOpacity, View} from 'react-native';

import { Cores } from '../../../Cores';

export default function CardPequeno({icon, titulo, onPress}) {
    return (
    <TouchableOpacity style={style.card} onPress={onPress} activeOpacity={0.8}>
        <View style={{flexDirection: "row"}}>
            <Image source={icon} style={style.icon}/>
            <Text style={style.titulo}>{titulo}</Text>
        </View>
        <Image source={require('../../../assets/icons/arrow-right.png')} style={style.seta}/>
    </TouchableOpacity>
    )
}

const style = StyleSheet.create({
    card: {
        flexDirection: "row",
        elevation: 5,
        width: "100%",
        borderRadius: 10,
        padding: 20,
        backgroundColor: "#ffffff",
        marginBottom: 20,
        justifyContent: "space-between",
    },
    icon: {
        tintColor: Cores.primary,
        height: 24,
        width: 24,
    },
    titulo: {
        fontSize: 18,
        fontWeight: "bold",
        color: Cores.primary,
        marginLeft: 10,
    },
    seta: {
        width: 24,
        height: 24,
        tintColor: Cores.primary,
    }
})