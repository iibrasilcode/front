import React from 'react';
import Card from './Card';
import deposit from '../../../assets/icons/deposit.png';
import { useNavigation } from '@react-navigation/native';

export default function Entradas({entradas}) {
    const navigation = useNavigation();
    var date = new Date().toLocaleDateString('pt-BR');

    return <Card icon={deposit} titulo={"Entradas"} valor={'+ R$ '+entradas} atualizacao={date} onPress={()=> {navigation.navigate('Entradas')}}/>
}