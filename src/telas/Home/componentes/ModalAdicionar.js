import React, { useState } from "react";
import { Modal, StyleSheet, Text, View, TextInput, Image, TouchableOpacity, ToastAndroid} from "react-native";
//import DatePicker from "react-native-date-picker";
import MaskInput, { Masks } from 'react-native-mask-input';

import { Cores } from "../../../Cores";

import entradas from '../../../../mocks/entradas';
import saidas from '../../../../mocks/saidas';

export default function ModalAdicionar({setModal, visible, pagina, setRefresh}) {

    const [nomeNota, setNomeNota] = useState('');
    const [date, setDate] = useState(new Date().toLocaleDateString('pt-BR'));
    //const [open, setOpen] = useState(false);
    //const [text, setText] = useState(date.toDateString());

    const [money, setMoney] = React.useState('');

    function adicionar() {
        if(nomeNota==''||date ==''||money=='') {
            alert('Faltam dados!');
            return
        }
        ToastAndroid.show(`Adicionando ${money} em ${pagina}`, ToastAndroid.SHORT);
        switch (pagina) {
            case 'entradas':
                entradas.lista.push({
                    id: 1,
                    nome: nomeNota,
                    imagem: require('../../../assets/Home/default-placeholder.png'),
                    valor: money.replace('R$ ', '').replace('.','').replace(',','.'),
                    data: date.toString()
                })
                break;
        
            case 'saidas':
                saidas.lista.push({
                    id: 1,
                    nome: nomeNota,
                    imagem: require('../../../assets/Home/default-placeholder.png'),
                    valor: money.replace('R$ ', '').replace('.','').replace(',','.'),
                    data: date.toString()
                })
                break;
        }
        setModal(false);
        setRefresh();
    }

    return (
        <Modal
        animationType="slide"
        visible={visible}
        transparent={true}
        onRequestClose={()=> setModal(false)}
        >
            <View style={style.back}>
                <View style={style.container}>

                    <TouchableOpacity style={style.close} onPress={()=>setModal(false)}>
                        <Image source={require('../../../assets/icons/close.png')} style={style.closeModal}/>
                    </TouchableOpacity>

                    <View style={style.containerImg}>
                        <Image source={require('../../../assets/Home/default-placeholder.png')} style={style.img}/>
                        <TouchableOpacity activeOpacity={0.8} style={style.cameraBkg}>
                            <Image source={require('../../../assets/icons/camera.png')} style={style.camera}/>
                        </TouchableOpacity>
                    </View>
                    <Text style={style.texto}>Nome da nota:</Text>
                    <TextInput style={style.input} placeholder={"Insira o nome..."} onChangeText={setNomeNota} placeholderTextColor={Cores.input.placeholder} maxLength={45} selectionColor={Cores.input.selection}/>
                    <Text style={style.texto}>Valor da nota:</Text>

                    <MaskInput style={style.input} value={money} onChangeText={setMoney} mask={Masks.BRL_CURRENCY} keyboardType={"number-pad"} maxLength={13} placeholderTextColor={Cores.input.placeholder} selectionColor={Cores.input.selection}/>

                    <Text style={style.texto}>Data:</Text>
                    <MaskInput style={style.input} value={date} onChangeText={setDate} mask={Masks.DATE_DDMMYYYY} keyboardType={"number-pad"} placeholderTextColor={Cores.input.placeholder} selectionColor={Cores.input.selection}/>
                    {/* <Pressable onPress={()=>setOpen(true)}>
                        <TextInput 
                        style={style.input}
                        defaultValue={text}
                        
                        editable={false}/>    
                    </Pressable> 
                    <DatePicker 
                    modal open={open}
                    mode={"date"}
                    date={date}
                    locale={"pt-br"}
                    onDateChange={()=> {
                        setDate;
                        setText(date);
                        }
                    }
                    onConfirm={(date)=>{
                        setOpen(false)
                        setDate(date)
                    }}
                    onCancel={()=>{
                        setOpen(false)
                    }}/> */}
                    <TouchableOpacity style={[style.botao, {backgroundColor:Cores.primary}]} onPress={()=>adicionar()} activeOpacity={0.8}>
                        <Text style={{color: "#ffffff", fontWeight:"500"}}>Salvar</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={style.botao} onPress={()=>setModal(false)}>
                        <Text style={{color: Cores.primary, fontWeight:"500"}}>Cancelar</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
    )
}

const style = StyleSheet.create({
    back:{
        backgroundColor: "rgba(0,0,0,0.15)",
        width: "100%",
        height: "100%",
    },
    container: {
        flex: 1,
        width: 350,
        borderRadius: 10,
        position: "absolute",
        top: 100,
        alignSelf: "center",
        backgroundColor: "#ffffff",
        padding: 30,
    },
    close: {
        position: "absolute",
        right: 20,
        top: 20,
    },
    closeModal: {
        height: 24,
        width: 24,
    },
    containerImg: {
        marginBottom: 20,
        height: 100,
        width: 100, 
        alignSelf: "center",
    },
    img: {
        borderRadius: 10,
        height: 100,
        width: 100,
    },
    camera: {
        height: 24,
        width: 24,
        tintColor: "#ffffff",
    },
    cameraBkg: {
        backgroundColor: Cores.primary,
        borderRadius: 25,
        position: "absolute",
        bottom: -10,
        right: -10,
        padding: 4,
    },
    texto: {
        fontSize: 14,
        fontWeight: "500",
        color: Cores.primary,
        marginBottom: 5,
    },
    input: {
        backgroundColor: Cores.input.background,
        borderRadius: 6,
        paddingHorizontal: 15,
        marginBottom: 10,
        color: Cores.primary,
    },
    botao: {
        padding: 12,
        alignItems: "center",
        width: "100%",
        borderRadius: 10,
    }
})