import React from 'react';
import {StatusBar, StyleSheet} from 'react-native';
import Header from '../componentes/Header';

import { Cores } from '../../../Cores';

export default function Resultado() {
    return ( 
    <>
        <StatusBar backgroundColor={Cores.primaryDarker} barStyle={'light-content'}/>
        <Header icon={require('../../../assets/icons/us-dollar-circled.png')} titulo={"Resultado"} descricao={"Este é o dinheiro que sobrou"} azul={true}/>
    </>
    )
}

const style = StyleSheet.create({

})