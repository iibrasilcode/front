import React from 'react'
import {View, FlatList, StyleSheet, RefreshControl, Text} from 'react-native'
import useNotificacoes from '../../../../hooks/useNotificacoes';
import { Cores } from '../../../Cores'
import Notificacao from './Notificacao';
import Header from '../componentes/Header'

export default function Notificacoes() {
    const [ lista, updateNotificacoes] = useNotificacoes();

    const [refreshing, setRefreshing] = React.useState(false);
    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        setTimeout(() => {
            updateNotificacoes();
            setRefreshing(false);
        }, 700);
    }, []);

    const HeaderN = () => {
        return <Header icon={require('../../../assets/icons/notification.png')} titulo={"Notificações"} descricao={'Veja suas notificações'}/>
    }
    const Footer = () => {
        return <Text style={style.footer}>Não há notificações abaixo.</Text>
    }

    return (
        <View style={{flex: 1}}>
            <FlatList
            style={style.lista}
            data={lista}
            renderItem={({item})=> <Notificacao {...item}/>}
            keyExtractor={(item, index)=>String(index)}
            ListHeaderComponent={HeaderN}
            ListFooterComponent={Footer}
            refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh}/>}
            />
        </View>
    )
}

const style = StyleSheet.create({
    lista: {
        backgroundColor: Cores.backgroundLista
    },
    footer: {
        textAlign: 'center',
        color: Cores.description.primary,
        marginVertical: 15,
        backgroundColor: Cores.backgroundLista,
        fontWeight: '500'
    }
})