import React, {useState} from 'react';
import {Text,View, StyleSheet, TouchableOpacity} from 'react-native';

import { Cores } from '../../../Cores';

export default function Notificacao({titulo,descricao, data}) {
    const [selected, setSelect] = useState(false);

    return (
    <TouchableOpacity 
        style={[style.card, {backgroundColor: !selected?"#fff":Cores.backgroundLista}]}
        onPress={()=>setSelect(!selected)}
        activeOpacity={0.7}
        >
        <View style={style.info}>
            <View style={{width: "85%", }}>
                <Text style={style.titulo}>{titulo}</Text>
                <Text style={[style.descricao, {display: descricao?'flex':'none'}]}>{descricao}</Text>
            </View>
            <Text style={style.data}>{data}</Text>
        </View>
    </TouchableOpacity>
    )
}

const style = StyleSheet.create({
    card: {
        flexDirection: "row",
        backgroundColor: "#F9F9FB",
        padding: 16,
        borderColor: "#dddddd",
        borderBottomWidth: 1,
    },
    info: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between",
        marginLeft: 8,
        alignItems: "center",
    },
    titulo: {
        fontSize: 16,
        fontWeight: "bold",
        color: Cores.primary,
        //textAlign: "justify"
    },
    descricao: {
        fontSize: 14,
        fontWeight: "500",
        color: Cores.description.primary,
        //textAlign: "justify"
    },
    data: {
        fontSize: 14,
        fontWeight: "bold",
        color: Cores.primary,
    }
})