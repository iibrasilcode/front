import React, {useState} from 'react';
import {StyleSheet, FlatList, Text, RefreshControl, ToastAndroid} from 'react-native';
import Header from '../componentes/Header';

import useSaidas from '../../../../hooks/useSaidas';

import Saida from './Saida';
import BotaoAdicionar from '../componentes/BotaoAdicionar';
import ModalAdicionar from '../componentes/ModalAdicionar';
import Total from '../componentes/Total';

import { Cores } from '../../../Cores';

export default function Saidas() {
    const [titulo, descricao, lista, total, updateSaidas] = useSaidas();
    const [open, setModal] = useState(false);

    //refresh lista
    const [refreshing, setRefreshing] = React.useState(false);
    const [listData, setListData] = React.useState(lista);
    const onRefresh = React.useCallback(async () => {
        setRefreshing(true);
        if (listData.length < 10) {
          try {
            setTimeout(() => {
              updateSaidas();
              setRefreshing(false)
            }, 1000);
          } catch (error) {
            console.error(error);
          }
        }
        else{
          ToastAndroid.show('No more new data available', ToastAndroid.SHORT);
          setRefreshing(false)
        }
      }, [refreshing]);


    const HeaderSaidas = () => {
        return (
        <>
        <Header icon={require('../../../assets/icons/withdrawal.png')} titulo={titulo} descricao={descricao}/>
        <BotaoAdicionar texto={"Adicionar saída:"} valor={"R$ 0,00"} onPress={()=>setModal(true)}/>
        <Total texto={"Total de saídas: "} valor={"- R$ "+total} ano={"2023"} corValor={Cores.money.out}/>
        </>
        )
    }

    const FooterSaidas = () => {
        return (
            <Text style={style.footer}>Não há perdas abaixo</Text>
        )
    }

 return (
    <>
    <FlatList
    style={style.lista}
    data={lista}
    renderItem={({item})=> <Saida {...item}/>}
    keyExtractor={(item, index)=>String(index)}
    ListHeaderComponent={HeaderSaidas}
    ListFooterComponent={FooterSaidas}
    refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh}/>}
    />
    <ModalAdicionar setModal={setModal} visible={open} pagina={'saidas'} setRefresh={onRefresh}/>
    </>
 )
}

const style = StyleSheet.create({
    lista: {
        backgroundColor: Cores.backgroundLista,
    },
    footer: {
        textAlign: "center",
        color: Cores.description.primary,
        fontSize: 14,
        marginVertical: 15,
        fontWeight: '500',
    },
})