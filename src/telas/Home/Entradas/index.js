import React, {useState} from 'react';
import {StyleSheet, FlatList, Text, RefreshControl, ToastAndroid} from 'react-native';
import Header from '../componentes/Header';

import useEntradas from '../../../../hooks/useEntradas';

import Entrada from './Entrada';
import BotaoAdicionar from '../componentes/BotaoAdicionar';
import ModalAdicionar from '../componentes/ModalAdicionar';

import Total from '../componentes/Total';
import { Cores } from '../../../Cores';

export default function Entradas() {
    const [titulo, descricao, lista, total, updateEntradas] = useEntradas();

    const [open, setModal] = useState(false);

    //refresh lista
    const [refreshing, setRefreshing] = React.useState(false);
    const [listData, setListData] = React.useState(lista);
    const onRefresh = React.useCallback(async () => {
        setRefreshing(true);
        if (listData.length < 10) {
          try {
            setTimeout(() => {
              updateEntradas();
              setRefreshing(false);
            }, 1000);
          } catch (error) {
            console.error(error);
          }
        }
        else{
          ToastAndroid.show('No more new data available', ToastAndroid.SHORT);
          setRefreshing(false)
        }
      }, [refreshing]);

    const HeaderEntradas = () => {
        return (
        <>
        <Header icon={require('../../../assets/icons/deposit.png')} titulo={titulo} descricao={descricao}/>
        <BotaoAdicionar texto={"Adicionar ganho:"} valor={"R$ 0,00"} onPress={()=> setModal(true)} />
        <Total texto={"Total de entradas: "} valor={"+ R$ "+total} corValor={Cores.money.in} ano={"2023"}/>
        </>
        )
    }

    const FooterEntradas = () => {
        return (
            <Text style={style.footer}>Não há ganhos abaixo</Text>
        )
    }

 return (
  <>
  <FlatList
  style={style.lista}
  data={lista}
  renderItem={({item})=> <Entrada {...item}/>}
  keyExtractor={(item, index)=>String(index)}
  ListHeaderComponent={HeaderEntradas}
  ListFooterComponent={FooterEntradas}
  refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh}/>}
  />
  <ModalAdicionar setModal={setModal} visible={open} pagina={'entradas'} setRefresh={onRefresh}/>
  </>  
 )
}

const style = StyleSheet.create({
    lista: {
        backgroundColor: Cores.backgroundLista,
    },
    footer: {
        textAlign: "center",
        color: Cores.description.primary,
        fontSize: 14,
        marginVertical: 15,
        fontWeight: '500',
    },
})