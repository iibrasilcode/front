import React, {useState} from 'react';
import {Text,View,Image, StyleSheet, TouchableOpacity} from 'react-native';

import { Cores } from '../../../Cores';

export default function Entrada({nome,imagem,data,valor}) {
    const [selected, setSelect] = useState(false);

    return (
    <TouchableOpacity 
        style={style.card}
        onPress={()=>setSelect(!selected)}
        activeOpacity={0.7}
        >
        <Image source={imagem} style={style.imagem} accessibilityLabel={nome}/>
        <View style={style.info}>
            <View>
                <Text style={style.nome}>{nome}</Text>
                <Text style={style.valor}>+ R$ {valor}</Text>
            </View>
            <Text style={style.data}>{data}</Text>
        </View>
    </TouchableOpacity>
    )
}

const style = StyleSheet.create({
    card: {
        flexDirection: "row",
        backgroundColor: "#F9F9FB",
        padding: 16,
        borderColor: "#dddddd",
        borderBottomWidth: 1,
    },
    imagem: {
        width: 40,
        height: 40,
        borderRadius: 6,
    },
    info: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between",
        marginLeft: 8,
        alignItems: "center",
    },
    nome: {
        fontSize: 14,
        fontWeight: "bold",
        color: Cores.primary,
    },
    valor: {
        fontSize: 14,
        fontWeight: "bold",
        color: Cores.money.in,
    },
    data: {
        fontSize: 14,
        fontWeight: "bold",
        color: Cores.primary,
    }
})