import React from 'react';
import { ScrollView, StyleSheet, Text} from 'react-native';

import { Cores } from '../../Cores';

export default function Transacoes() {
    return <ScrollView style={style.container}>
        <Text style={style.text}>Nada aqui</Text>
    </ScrollView>
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
    },
    text: {
        color: "#000",
        fontSize: 18,
    }
})