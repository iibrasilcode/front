import React from 'react';
import {ScrollView, View, Text, StyleSheet,TouchableOpacity, Alert, TextInput, Dimensions} from 'react-native';
import { useNavigation } from '@react-navigation/native';

import { formatWithMask, Masks } from 'react-native-mask-input';

import { Cores } from '../../../Cores';

const width = Dimensions.get('screen').width;

export default function Adicionar({route}) {
    const navigation = useNavigation();

    const {data} = route.params!=null?route.params:'alo';
    const arr_data = data.split("|");

    function adicionar() {
        Alert.alert(
            'Nota adicionada!',
            'Exemplo de nota',
            [
              {
                text: 'OK',
                onPress: () => navigation.navigate("Camera"),
                style: 'default'
              }
            ],
            { cancelable: false }
        )
    }

    var date = new Date().toLocaleDateString('pt-BR');

    var valor = (Math.random()*180+20).toFixed(2).replace('.',',');

    return (
    <ScrollView>
        <View style={style.header}>
            <TextInput style={style.nome} placeholder={"Nome da Nota..."} placeholderTextColor={Cores.input.placeholder}
            multiline={true} maxLength={45} selectionColor={Cores.input.selection} defaultValue={data}/>
        </View>
        <View style={style.container}>
            <TouchableOpacity style={style.field} activeOpacity={0.7}>
                <Text style={style.fieldTxt}>Valor:</Text>
                <Text style={style.fieldTxt2}>R${valor}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={style.field} activeOpacity={0.7}>
                <Text style={style.fieldTxt}>Data:</Text>
                <Text style={style.fieldTxt2}>{date}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={style.btn} activeOpacity={0.7} onPress={()=>adicionar()}>
                <Text style={style.btnTxt}>Adicionar</Text>
            </TouchableOpacity>
        </View>
    </ScrollView>
    )
}

const style = StyleSheet.create({
    container: {
        paddingHorizontal: 20,
        paddingVertical: 20
    },
    header: {
        paddingVertical: 40,
        backgroundColor: "#fff",
        elevation: 5,
        flexShrink: 1,
    },
    nome: {
        textAlign: 'center',
        fontSize: 32,
        fontWeight: "bold",
        color: Cores.primary,
        paddingHorizontal: 20,
    },
    fieldTxt: {
        fontSize: 18,
        color: Cores.primary,
        //fontWeight: "500",
    },
    fieldTxt2: {
        fontSize: 32,
        color: Cores.primary,
        fontWeight: "500",
    },
    field: {
        backgroundColor: "#e9e9e9",
        padding: 15,
        borderRadius: 10,
        marginTop: 10,
    },
    btn: {
        backgroundColor: Cores.primary,
        padding: 15,
        borderRadius: 10,
        marginTop: 10,
    },
    btnTxt: {
        fontSize: 18,
        color: "#fff",
        textAlign: 'center',
        fontWeight: "500",
    }
})