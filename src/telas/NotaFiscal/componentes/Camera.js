import React, {useState} from 'react';
import {View, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {RNCamera} from 'react-native-camera';

import { useNavigation } from '@react-navigation/native';

import Header from './Header';

export default function Camera() {

    const navigation = useNavigation();

    const [flash, setFlash] = useState(false);
    const style = styleFunc(flash);

    function barcodeRecognized(type,data,rawData) {
        if(type!='QR_CODE') return;
        var arr_data = data.split("|");

        
        //alert(data);    
        navigation.navigate("TelaAdicionar", {data: data});
    }

    return (
    <View style={style.container}>
        <Header showClose={false}/>
        <RNCamera
        //ref={ref => {this.camera = ref}}
        onDoubleTap={()=>navigation.navigate("TelaAdicionar", {data: ''})}
        style={style.scanner}
        onBarCodeRead={({data,type,rawData})=>barcodeRecognized(type,data,rawData)}
        captureAudio={false}
        flashMode={flash?"torch":"off"}>
            <View style={style.target}></View>
            <TouchableOpacity style={style.flashBtn} onPress={()=>setFlash(!flash)} activeOpacity={0.8}>
                <Image source={require('../../../assets/NotaFiscal/flashlight.png')} style={style.flashImg}/>
            </TouchableOpacity>
        </RNCamera>
    </View>
    )
}

const styleFunc = (flash) => StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'black',
    },
    scanner: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    target: {
        position: "absolute",
        width: 300,
        height: 300,
        borderWidth: 8,
        borderColor: 'rgba(255,255,255,0.5)',
        borderRadius: 20
    },
    flashBtn: {
        padding: 10,
        bottom: 30,
        backgroundColor: flash?"rgba(255,255,150,0.6)":"rgba(255,255,255,0.6)",
        position: "absolute",
        borderRadius: 20,
    },
    flashImg: {
        width:40,
        height: 40,
        tintColor: flash?"rgba(255,255,30,0.8)":"rgba(255,255,255,0.7)",
    }
})