import React from 'react';

import Header from './Header';
import Adicionar from './Adicionar';

export default function TelaAdicionar({setCamera, route}) {
    return (
        <>
        <Header setCamera={setCamera}/>
        <Adicionar setCamera={setCamera} route={route}/>
        </>
    )
}