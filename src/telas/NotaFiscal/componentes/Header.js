import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Image} from 'react-native';
import { useNavigation } from '@react-navigation/native';

import { Cores } from '../../../Cores';

export default function Header({showClose = true}) {
    const navigation = useNavigation();
    return (
        <View>
            <View style={style.header}>
                <Text style={style.texto}>Adicionar Nota</Text>
                <TouchableOpacity style={[style.closeBtn, {display: showClose?"flex":"none"}]} activeOpacity={0.7} onPress={()=>navigation.navigate("Camera")}>
                    <Image source={require('../../../assets/icons/close.png')} style={style.closeImg}/>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const style = StyleSheet.create({
    header: {
        backgroundColor:"#fff",
        paddingHorizontal:15,
        paddingVertical:15,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    texto: {
        fontWeight: 'bold',
        fontSize:20,
        color: Cores.primary,
    },
    closeBtn: {
        position: 'absolute',
        right: 10,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#ffffff",
        width: 32,
        height: 32,
        borderRadius: 10,
    },
    closeImg: {
        height: 24,
        width: 24,
        tintColor: Cores.primary,
    },
})