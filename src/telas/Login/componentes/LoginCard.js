import React, {useState} from "react";
import {View, Image, Text, TextInput, StyleSheet, ActivityIndicator, TouchableOpacity} from 'react-native'
import { useNavigation, StackActions } from '@react-navigation/native';
import Botao from './Botao';
import { Cores } from "../../../Cores";

export default function LoginCard({setModal}) {
    const navigation = useNavigation();

    const [usuario, setUsuario] = useState('Felipe');
    const [senha, setSenha] = useState('senhaforte');
    const [loading, setLoading] = useState(false);
    
    function replaceScreen(screen) {
        navigation.dispatch(
            StackActions.replace(screen)
        );
    }

    function fazerLogin() {
        if(usuario==''||usuario==null) {
            alert('Digite um nome de usuário!');
            return;
        }
        if (senha==''||senha==null) {
            alert('Digite uma senha!');
            return;
        }
        setLoading(true);
        global.usuario = usuario
        setTimeout(() => {
            replaceScreen("Home")
        }, 1500);
    }

    return (
        <View style={style.login}>
            <View style={{flexDirection: 'row', alignSelf: 'center', alignItems: 'center', justifyContent: 'center', width: 200}}>
                <ActivityIndicator color={Cores.primary} size={40} animating={loading} style={{position:'absolute', left:0}}/>
                <Image source={require('../../../assets/logo/iibr.png')} style={{width: 48, height: 48, tintColor: Cores.primary, marginBottom: 5}}/>
                <Text style={{fontSize: 42, color: "#000", textAlign: 'center', marginBottom: 10, color: Cores.primary}}>Sou</Text>
            </View>
            <TextInput style={style.input} placeholder="Usuário" defaultValue={usuario} placeholderTextColor={Cores.input.placeholder} selectionColor={Cores.input.selection} onChangeText={newText => setUsuario(newText)} maxLength={20} editable={!loading}/>
            <TextInput style={style.input} placeholder="Senha" defaultValue={senha} secureTextEntry={true} placeholderTextColor={Cores.input.placeholder} selectionColor={Cores.input.selection} onChangeText={newText => setSenha(newText)} editable={!loading}/>
            <TouchableOpacity style={{marginVertical: 5}} activeOpacity={0.8} onPress={()=> alert("Esqueceu a senha")}>
                <Text style={{color: Cores.primary, fontWeight: '500'}}>Esqueceu sua senha?</Text>
            </TouchableOpacity>
            <View style={{flexDirection: 'row', justifyContent:'space-between'}}>
                <Botao buttonStyle={style.buttonLeft} textStyle={style.textButtonLeft} text="Cadastro" onPress={()=>setModal(true)}/>
                <Botao buttonStyle={style.buttonRight} textStyle={style.textButtonRight} text="Login" onPress={()=>fazerLogin()}/>
            </View>
        </View>
    )
}

const style = StyleSheet.create({
    login: {
        backgroundColor: "#fff",
        padding: 10,
        borderRadius: 15,
        elevation: 5,
    },
    input: {
        padding: 10,
        backgroundColor: Cores.input.background,
        color: "#000",
        borderRadius: 12,
        width: 300,
        fontSize: 18,
        marginBottom: 5,
        //textAlign: 'center',
    },
    buttonLeft: {
        width: 300/2,
        borderBottomLeftRadius: 12,
        borderTopLeftRadius: 12,
        borderBottomRightRadius: 0,
        borderTopRightRadius: 0,
        backgroundColor: "#fff",
        borderColor: Cores.primary,
        borderWidth: 1,
    },
    buttonRight: {
        width: 300/2+1,
        borderBottomLeftRadius: 0,
        borderTopLeftRadius: 0,
        borderBottomRightRadius: 12,
        borderTopRightRadius: 12,
        borderColor: Cores.primary,
        borderWidth: 1,
    },
    textButtonLeft: {
        fontSize: 18,
        color: Cores.primary,
        fontWeight: '500',
    },
    textButtonRight: {
        fontSize: 18,
        fontWeight: '500',
    }
})