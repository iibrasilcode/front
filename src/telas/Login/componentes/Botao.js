import React from 'react';
import {Text, StyleSheet, TouchableOpacity} from 'react-native';

import { Cores } from '../../../Cores';

export default function Botao({text="text", onPress, buttonStyle, textStyle}){
    return (
        <TouchableOpacity style={[styles.button, buttonStyle]} activeOpacity={0.8} onPress={onPress}>
            <Text style={[{color: "#fff", textAlign: 'center'}, textStyle]} >{text}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    button: {
        padding: 10,
        borderRadius: 5,       
        backgroundColor: Cores.primary,        
    }
})