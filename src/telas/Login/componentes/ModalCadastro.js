import React, {useState} from "react";
import {Modal, View, Text, StyleSheet, TextInput, Dimensions, Image, ActivityIndicator} from "react-native";
import MaskInput, { Masks } from 'react-native-mask-input';
import Botao from "./Botao";
import { Cores } from "../../../Cores";

const windowHeight = Dimensions.get('window').height;

export default function ModalCadastro({modalVisible, setModal}){
    const [loading, setLoading] = useState(false)

    const [usuario, setUsuario] = useState('')
    const [email, setEmail] = useState('')
    const [senha, setSenha] = useState('')
    const [senha2, setSenha2] = useState('')
    const [nascimento, setNascimento] = useState('')

    function fazerCadastro() {
        if(usuario==''||usuario==null) {
            alert('Digite um nome de usuário!');
            return;
        }
        if(email==''||email==null) {
            alert('Digite um email!');
            return;
        }
        if (senha==''||senha==null) {
            alert('Digite uma senha!');
            return;
        }
        if (senha!=senha2) {
            alert('As senhas são diferentes!');
            return;
        }
        if (nascimento.length!=10) {
            alert('Digite uma data de nascimento válida!');
            return;
        }
        setLoading(true);

        setTimeout(() => {
            setEmail('')
            setSenha('')
            setSenha2('')
            setNascimento('')

            setLoading(false)
            setModal(false)
        }, 1500);

    }

    return (
        <Modal visible={modalVisible} onRequestClose={()=>setModal(false)} animationType={'fade'} transparent={false} style={{flex:1, alignItems: "center"}}>
            <View style={style.back}>
                <Image source={require('../../../assets/Login/background.png')} style={style.background}/>
                <View style={style.container}>
                    <View style={{flexDirection: 'row', alignSelf: 'center', alignItems: 'center', justifyContent: 'center', width: 300}}>
                        <ActivityIndicator color={Cores.primary} size={40} animating={loading} style={{position:'absolute', left:0}}/>
                        <Image source={require('../../../assets/logo/iibr.png')} style={{width: 48, height: 48, tintColor: Cores.primary, marginBottom: 5}}/>
                        <Text style={{fontSize: 42, color: "#000", textAlign: 'center', marginBottom: 10, color: Cores.primary}}>Cadastro</Text>
                    </View>

                    <Text style={style.label}>Nome de Usuário:</Text>
                    <TextInput style={style.input} value={usuario} onChangeText={setUsuario} placeholder={"Usuário"} placeholderTextColor={Cores.input.placeholder} selectionColor={Cores.input.selection}/>

                    <Text style={style.label}>E-mail:</Text>
                    <TextInput style={style.input} value={email} onChangeText={setEmail} keyboardType={"email-address"} placeholder={"E-mail"} placeholderTextColor={Cores.input.placeholder} selectionColor={Cores.input.selection}/>
                    
                    <Text style={style.label}>Senha:</Text>
                    <TextInput style={style.input} value={senha} onChangeText={setSenha} secureTextEntry={true} placeholder={"Senha"} placeholderTextColor={Cores.input.placeholder} selectionColor={Cores.input.selection}/>
                    
                    <Text style={style.label}>Repita a Senha:</Text>
                    <TextInput style={style.input} value={senha2} onChangeText={setSenha2} secureTextEntry={true} placeholder={"Senha"} placeholderTextColor={Cores.input.placeholder} selectionColor={Cores.input.selection}/>
                    
                    <Text style={style.label}>Data de Nascimento:</Text>
                    <MaskInput style={style.input} value={nascimento} onChangeText={setNascimento} mask={Masks.DATE_DDMMYYYY} keyboardType={"number-pad"} placeholderTextColor={Cores.input.placeholder} selectionColor={Cores.input.selection}/>

                    <View style={{flexDirection: 'row', justifyContent:'space-between'}}>
                        <Botao buttonStyle={style.buttonLeft} textStyle={style.textButtonLeft} text="Cancelar" onPress={()=>setModal(false)}/>
                        <Botao buttonStyle={style.buttonRight} textStyle={style.textButtonRight} text="Cadastrar" onPress={()=>fazerCadastro()}/>
                    </View>
                </View>
            </View>
        </Modal>
    )
}

const style = StyleSheet.create({
    back: {
        flex: 1,
        justifyContent: "center",
        backgroundColor: "#f2f2f2",
    },
    container: {
        alignSelf: "center",
        padding: 10,
        backgroundColor: "#fff",
        width: 350,
        borderRadius: 15,
        elevation: 5,
    },
    label: {
        color: Cores.primary,
        fontSize: 14,
        fontWeight: "500",
        paddingHorizontal: 10,
    },
    buttonLeft: {
        width: 330/2,
        borderBottomLeftRadius: 12,
        borderTopLeftRadius: 12,
        borderBottomRightRadius: 0,
        borderTopRightRadius: 0,
        backgroundColor: "#fff",
        borderColor: Cores.primary,
        borderWidth: 1,
    },
    buttonRight: {
        width: 330/2,
        borderBottomLeftRadius: 0,
        borderTopLeftRadius: 0,
        borderBottomRightRadius: 12,
        borderTopRightRadius: 12,
        borderColor: Cores.primary,
        borderWidth: 1,
    },
    textButtonLeft: {
        fontSize: 18,
        color: Cores.primary,
        fontWeight: '500',
    },
    textButtonRight: {
        fontSize: 18,
        fontWeight: '500',
    },
    input: {
        backgroundColor: Cores.input.background,
        borderRadius: 12,
        color: Cores.primary,
        marginBottom: 5,
        padding: 10,
        fontSize: 16,
    },
    background: {
        position: "absolute",
        right: 0,
        top: 0,
        height: "100%",
        width: windowHeight*0.536,
        opacity: 0.2,
    },
})