import React, {useState} from 'react';
import { SafeAreaView, StatusBar,StyleSheet, Dimensions, Image} from 'react-native';

import LoginCard from './componentes/LoginCard';
import ModalCadastro from './componentes/ModalCadastro';

const windowHeight = Dimensions.get('window').height;

export default function Login() {
    
    const [modalVisible, setModal] = useState(false);
    
    return (
        <SafeAreaView style={style.container}>
            <StatusBar backgroundColor={"#eee"}/>
            <Image source={require('../../assets/Login/background.png')} style={style.background}/>
            <ModalCadastro modalVisible={modalVisible} setModal={setModal}/>
            <LoginCard setModal={setModal}/>
        </SafeAreaView>
    )
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#f2f2f2",
        alignItems: 'center',
        justifyContent: 'center'
        
    },
    background: {
        position: "absolute",
        right: 0,
        top: 0,
        height: "100%",
        width: windowHeight*0.536,
        opacity: 0.2,
    },
})