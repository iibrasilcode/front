import defaultImg from '../src/assets/Home/default-placeholder.png';

const entradas = {
    titulo: "Entradas",
    descricao: "Informe seus ganhos ativos e passivos",
    lista: [
        {
            id: 1,
            imagem: defaultImg,
            nome: "Aluguel da Casa Barequeçaba",
            valor: (400).toFixed(2),
            data: "19/06/2022"
        },
        {
            id: 2,
            imagem: defaultImg,
            nome: "Salário do trabalho",
            valor: (400).toFixed(2),
            data: "16/06/2022"
        },
        {
            id: 3,
            imagem: defaultImg,
            nome: "Trabalho Freelancer",
            valor: (200).toFixed(2),
            data: "11/06/2022"
        },
        {
            id: 4,
            imagem: defaultImg,
            nome: "Trabalho Freelancer",
            valor: (270).toFixed(2),
            data: "15/06/2022"
        },
        {
            id: 5,
            imagem: defaultImg,
            nome: "Trabalho Freelancer",
            valor: (290).toFixed(2),
            data: "14/06/2022"
        },
        {
            id: 6,
            imagem: defaultImg,
            nome: "Trabalho Freelancer",
            valor: (306).toFixed(2),
            data: "13/06/2022"
        },
        {
            id: 7,
            imagem: defaultImg,
            nome: "Trabalho Freelancer",
            valor: (234).toFixed(2),
            data: "19/06/2022"
        },
        {
            id: 8,
            imagem: defaultImg,
            nome: "Trabalho Freelancer",
            valor: (185).toFixed(2),
            data: "10/06/2022"
        },
        {
            id: 9,
            imagem: defaultImg,
            nome: "Trabalho Freelancer",
            valor: (271).toFixed(2),
            data: "9/06/2022"
        },
        {
            id: 10,
            imagem: defaultImg,
            nome: "Trabalho Freelancer",
            valor: (323).toFixed(2),
            data: "8/06/2022"
        },
    ]
}

export default entradas;