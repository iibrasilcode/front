const notificacoes = {
    lista: [
        {
            id: 1,
            titulo: "Lorem Ipsum",
            descricao: "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit",
            data: "05/01"
        },
        {
            id: 2,
            titulo: "Nullam ullamcorper sapien felis, nec eleifend purus",
            descricao: "Praesent lacus est, ultrices a porttitor ut, finibus a diam",
            data: "05/01"
        },
        {
            id: 3,
            titulo: "Aenean et justo at justo lacinia blandit. Suspendisse.",
            data: "05/01"
        },
        {
            id: 4,
            titulo: "Curabitur congue mauris leo, at tempor lorem pretium",
            descricao: "Donec ut nisi sollicitudin orci bibendum ultrices. Duis vitae nibh.",
            data: "04/01",
        },
        {
            id: 5,
            titulo: "Proin maximus lacus sit amet mi accumsan, non",
            data: "04/01",
        },
        {
            id: 6,
            titulo: "In semper augue ac tincidunt consectetur, nulla molestie nunc",
            data: "04/01",
        },
        {
            id: 7,
            titulo: "Curabitur urna quam, interdum eget orci nec, facilisis suscipit",
            descricao: "Proin pellentesque massa eget ullamcorper pellentesque. Donec efficitur tempor nunc, dapibus.",
            data: "03/01",
        },
        {
            id: 8,
            titulo: "Donec mollis nisl ac ex pharetra",
            data: "03/01",
        },
    ]
}

export default notificacoes;