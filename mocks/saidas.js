import defaultImg from '../src/assets/Home/default-placeholder.png';

const saidas = {
    titulo: "Saídas",
    descricao: "Informe suas perdas ativas e passivas",
    lista: [
        {
            imagem: defaultImg,
            nome: "Conta de Água",
            valor: (100).toFixed(2),
            data: "20/06/2022"
        },
        {
            imagem: defaultImg,
            nome: "Escola do Pedro",
            valor: (150).toFixed(2),
            data: "16/06/2022"
        },
        {
            imagem: defaultImg,
            nome: "iFood",
            valor: (50).toFixed(2),
            data: "15/06/2022"
        },
        {
            imagem: defaultImg,
            nome: "Netflix",
            valor: (50).toFixed(2),
            data: "15/06/2022"
        },
        {
            imagem: defaultImg,
            nome: "Uber",
            valor: (50).toFixed(2),
            data: "09/06/2022"
        },
        {
            imagem: defaultImg,
            nome: "Gasolina",
            valor: (190).toFixed(2),
            data: "12/06/2022"
        },  
        {
            imagem: defaultImg,
            nome: "Gasolina",
            valor: (230).toFixed(2),
            data: "07/06/2022"
        },  
        {
            imagem: defaultImg,
            nome: "Gasolina",
            valor: (194).toFixed(2),
            data: "28/06/2022"
        },  
        {
            imagem: defaultImg,
            nome: "Gasolina",
            valor: (144).toFixed(2),
            data: "06/06/2022"
        },  
        {
            imagem: defaultImg,
            nome: "Gasolina",
            valor: (200).toFixed(2),
            data: "16/06/2022"
        },  
    ]
}

export default saidas;