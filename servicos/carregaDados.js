import entradas from '../mocks/entradas';
import saidas from '../mocks/saidas';
import notificacoes from '../mocks/notificacoes';

export const carregaEntradas = () => {
    return entradas;
}

export const carregaSaidas = () => {
    return saidas;
}

export const carregaNotificacoes = () => {
    return notificacoes;
}